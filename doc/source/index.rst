#####################################################
EnMAP-Box 3 Helmholtz-Zentrum Geesthacht Applications
#####################################################

Hi Martin, würde vorschlagen, dass du dich von der Struktur her an der LMU orientierst:
https://enmap-box-lmu-vegetation-apps.readthedocs.io/en/latest/index.html

**EnMAP and Agriculture:**

EnMAP enables...

**EnMAP-Box 3 Agricultural Applications:**

In order to use IVVRM, the EnMAP-Box 3.0 or higher...

**Related websites**

- Environmental Mapping and Analysis Program (EnMAP): `www.enmap.org <http://www.enmap.org/>`_
- Bitbucket source code repository: ...
- Uni Trier ...


|
|

**This documentation is structured as follows:**

.. toctree::
    :maxdepth: 2
    :caption: General

    general/contact.rst

..  toctree::
    :maxdepth: 2
    :caption: Applications

    apps/onns.rst

..  toctree::
    :maxdepth: 2
    :caption: Tutorials

    tuts/tut1.rst
