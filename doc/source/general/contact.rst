#######
Contact
#######

**E-Mail:**

- Matthias Wocher: m.wocher@lmu.de
- Martin Danner: m.danner@iggf.geo.uni-muenchen.de

**Further information:**

- EnMAP mission, www.enmap.org
- LMU Department of Geography, www.geographie.uni-muenchen.de
